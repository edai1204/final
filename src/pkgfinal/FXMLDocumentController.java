/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkgfinal;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 *
 * @author CSIE
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private Button button;
    private Connection Conn;
    @FXML
    private void handleButtonAction(ActionEvent event) {
        set();
        label.setText("Select"+doinsert()+"rows");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }   
    private int doinsert(){
        int rows=0;
        PreparedStatement pps=null;
        String insert=" INSERT INTO  EDDYNI.COLLEAGUES VALUES(?,?,?,?,?,?,?) ";
        try{
            if(Conn==null){
                set();
            }
            pps=Conn.prepareStatement(insert);
            pps.setInt(1,5);
            pps.setString(2, "Ni");
            pps.setString(3, "An");
            pps.setString(4, "music");
            pps.setString(5, "musician");
            pps.setString(6, "edai1204@gmail.com");
            pps.setInt(7,0);
            rows=pps.executeUpdate();
            
        }catch(SQLException ex){
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE,null,ex);
        }finally{
            try{
                pps.close();
                Conn.close();
            }catch(SQLException ex){
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE,null,ex);
            }
        }
        return rows;
    }
    private int doselect(){
        int rows=0;
        PreparedStatement pps=null;
        ResultSet result=null;
        String select="Select * From EDDYNI.COLLEAGUES";
        try{
            if(Conn==null){
                set();
            }
            pps=Conn.prepareStatement(select);
            result=pps.executeQuery();
            while(result.next()){
                System.out.println(result.getString("FIRSTNAME"));
                rows++;
            }
        }catch(SQLException ex){
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE,null,ex);
        }finally{
            try{
                result.close();
                pps.close();
                Conn.close();
            }catch(SQLException ex){
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE,null,ex);
            }
        }
            return rows;
    }
    private void set(){
        try{
        Conn = DriverManager.getConnection("jdbc:derby://localhost:1527/contect", "EddyNi", "ee123456789");
        if(Conn != null){
            label.setText("linked");
        }
        }catch(SQLException ex){
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE,null,ex);
            label.setText("unlinked");
        }
    }
    
}
